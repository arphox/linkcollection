# Math
- [BetterExplained](https://betterexplained.com/) - Math lessons that click

# Science
**YouTubers**
- [Kurzgesagt – In a Nutshell](https://www.youtube.com/c/inanutshell/videos) - Great science videos

# General / soft learning
## YouTubers
- [CrashCourse](https://www.youtube.com/user/crashcourse/videos)

## Websites
- [HowStuffWorks](https://www.howstuffworks.com/) - Learn How Everything Works

# Documentary
- [Curiosity Stream](https://curiositystream.com/)
- [Documentary Mania](https://www.documentarymania.com/) - free documentaries

# Meta
- [Effective learning: Twenty rules of formulating knowledge](https://www.supermemo.com/en/archives1990-2015/articles/20rules)
- [The Feynman Learning Technique](https://fs.blog/2021/02/feynman-learning-technique/)
- [How to Remember What You Read](https://fs.blog/2021/08/remember-books/)
- [How To Learn Stuff Quickly](https://www.joshwcomeau.com/blog/how-to-learn-stuff-quickly)
  - [How To Remember Anything Forever-ish](https://ncase.me/remember/)
- swyx
  - [Learn In Public](https://www.swyx.io/learn-in-public/)
