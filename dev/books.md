# Ajánlottak

A nevek előtt minden egyes + jel azt jelenti, hogy azt a könyvet említették még valahol máshol is. Amivel sokszor találkozok, azt előrébb kéne priorizálni, ezért érdemes ezt vezetni.

## Általános
- [Grokking Simplicity: Taming complex software with functional thinking: Normand, Eric: 9781617296208: Amazon.com: Books](https://www.amazon.com/Grokking-Simplicity-software-functional-thinking/dp/1617296201)
- Senior Engineer Mindset - Swizec Teller
- +[A Philosophy of Software Design by John Ousterhout](https://web.stanford.edu/~ouster/cgi-bin/book.php)
- [Working Effectively with Legacy Code](https://www.amazon.com/Working-Effectively-Legacy-Michael-Feathers/dp/0131177052)
- [Clean Architecture: A Craftsman’s Guide to Software Structure and Design](https://www.oreilly.com/library/view/clean-architecture-a/9780134494272/)
- [The Mythical Man-Month](https://www.amazon.com/Mythical-Man-Month-Software-Engineering-Anniversary/dp/0201835959)
- [97 Things Every Programmer Should Know: Collective Wisdom from the Experts](https://www.amazon.co.uk/dp/0596809484?linkCode=gs2&tag=oreilly20-21)
- [**First 90 Days**, Updated and Expanded: Critical Success Strategies for New Leaders at All Levels](https://www.amazon.co.uk/dp/1422188612/)
- [The Staff Engineer's Path](https://www.oreilly.com/library/view/the-staff-engineers/9781098118723/)
- [On Writing Well, 30th Anniversary Edition: The Classic Guide to Writing Nonfiction](https://www.amazon.co.uk/dp/0060891548)
- The Principles of Product Development Flow: Second Generation Lean Product Development
- [Google - Site Reliability Engineering](https://sre.google/sre-book/table-of-contents/) - **nagyon** el kéne olvasni!
- [Implementing Lean Software Development: From Concept to Cash](https://www.amazon.com/Implementing-Lean-Software-Development-Concept/dp/0321437381)
- [Refactoring to Patterns](https://www.google.com/search?client=firefox-b-d&q=refactoring+to+patterns)
- Coders at Work: Reflections on the Craft of Programming
- Think Like a Programmer: An Introduction to Creative Problem Solving
- [How to Design Programs, Second Edition (Matthias Felleisen)](https://htdp.org/2019-02-24/) - online olvasható, free
- [Code(Charles Petzold)](https://www.goodreads.com/book/show/44882.Code)
- [The Pragmatic Programmer: From Journeyman to Master(Andrew Hunt, David Thomas)](https://www.amazon.com/dp/020161622X/?tag=stackoverflow17-20)
- [Pro Git](https://git-scm.com/book/en/v2) - free
- [20 Most-Recommended Books for Software Developers](https://dev.to/awwsmm/20-most-recommended-books-for-software-developers-5578)
- [The Coding Career Handbook](https://learninpublic.org/)
- [Code Complete: A Practical Handbook of Software Construction, Second Edition 2nd Edition]
- [Accelerate: The Science of Lean Software and DevOps: Building and Scaling High Performing Technology Organizations: Forsgren PhD, Nicole, Humble, Jez, Kim bestselling author of The Phoenix Project The Unicorn Project and Wiring, Gene: 9781942788331: Amazon.com: Books](https://www.amazon.com/Accelerate-Software-Performing-Technology-Organizations/dp/1942788339)+
- Growing Object-Oriented Software, Guided by Tests
- The Leprechauns of Software Engineering
- Test-Driven Development By Example
- [The Programmer's Brain](https://www.manning.com/books/the-programmers-brain) - What every programmer needs to know about cognition
- [Functional Programming in C#, Second Edition](https://www.manning.com/books/functional-programming-in-c-sharp-second-edition) (lehet azóta van újabb verzió, ez 2021 dec)
- [System Design Interview – An insider's guide](https://www.amazon.com/System-Design-Interview-insiders-Second/dp/B08CMF2CQF)
- [System Design Interview – An Insider's Guide: Volume 2](https://www.amazon.com/System-Design-Interview-Insiders-Guide/dp/1736049119)
- [The Software Engineer's Guidebook: Navigating senior, tech lead, and staff engineer positions at tech companies and startups](https://www.amazon.com/Software-Engineers-Guidebook-Navigating-positions/dp/908338182X)
- [Designing Data-Intensive Applications: The Big Ideas Behind Reliable, Scalable, and Maintainable Systems](https://www.amazon.com/Designing-Data-Intensive-Applications-Reliable-Maintainable/dp/1449373321)
- [Hiring Senior Software Engineers doesn't have to be hard - Hiring Engineers](https://hiringengineersbook.com/book/)
- [The Clean Coder: A Code of Conduct for Professional Programmers (Robert C. Martin Series): Martin, Robert: 4708364241379: Amazon.com: Books](https://www.amazon.com/Clean-Coder-Conduct-Professional-Programmers/dp/0137081073)

## C# / .NET
- [Threading in C# (Joseph Albahari)](http://www.albahari.com/threading/) - ingyen ebook online
- [Pro .NET Memory Management (2018)](https://prodotnetmemory.com/)
- Concurrency in C# Cookbook, 2nd Edition - vagy ha van újabb verzió
- [xUnit Test Patterns: Refactoring Test Code](https://www.amazon.com/xUnit-Test-Patterns-Refactoring-Code/dp/0131495054)
- [Get Programming with F#: A guide for .NET developers](https://www.amazon.com/Get-Programming-guide-NET-developers/dp/1617293997)

## Testing
- [Unit Testing Principles, Practices, and Patterns (by. Vladimir Khorikov)](https://www.manning.com/books/unit-testing) ([link 2](https://enterprisecraftsmanship.com/book/)) - Effective testing styles, patterns, and reliable automation for unit testing, mocking, and integration testing with examples in C#

# Architect
- [The Art of Systems Architecting (Systems Engineering)](https://www.amazon.com/Art-Systems-Architecting-Engineering/dp/1420079131) - lehet hogy van újabb verziója azóta!

# Gyűjtemények
- [**Reading List of Gergely Orosz**](https://blog.pragmaticengineer.com/my-reading-list/)
  - [Holiday Book Recommendations for Engineering Managers, Software Engineers and Product Managers - The Pragmatic Engineer](https://blog.pragmaticengineer.com/holiday-tech-book-recommendations/)
- [20 Most-Recommended Books for Software Developers](https://dev.to/awwsmm/20-most-recommended-books-for-software-developers-5578)
- [All IT eBooks](http://www.allitebooks.com/)
- [GoalKicker.com](https://goalkicker.com/) - Free Programming Books, de inkább "Notes for Professionals"
  - Témakörök: .NET, Algorithms, Android, Angular 2+, AngularJS, Bash, C, C++, C#, CSS, Entity Framework, Excel VBA, Git, Haskell, Hibernate, HTML5, HTML5 Canvas, iOS Developer, Java, JavaScript, jQuery, Kotlin, LaTeX, Linux, MATLAB, Microsoft SQL Server, MongoDB, MySQL, Node.js, Objective-C, Oracle Database, Perl, PHP, PostgreSQL, PowerShell, Python, R, React JS, React Native, Ruby, Ruby on Rails, Spring Framework, SQL, Swift, TypeScript, VBA, Visual Basic .NET, Xamarin.Forms
- [DevFreeBooks](https://devfreebooks.github.io/) - mindenféle könyv link, mindenféle témában
- [The Architecture of Open Source Applications](http://aosabook.org/en/index.html)

# Cikkek
- [5 book recommendations for developers (Hackernoon)](https://hackernoon.com/5-book-recommendations-for-developers-1781df7ebdd1)
  1. Eloquent JavaScript: A Modern Introduction to Programming by Marijn Haverbeke
  1. You Don’t Know JS (book series) by Kyle Simpson
  1. The Complete Software Developer’s Career Guide by John Sonmez
  1. Clean Code: A Handbook of Agile Software Craftsmanship by Robert C. Martin
  1. Cracking the Coding Interview by Gayle Laakmann McDowell
