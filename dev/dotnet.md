# Article archive
- [How to resolve .NET reference and NuGet package version conflicts](https://michaelscodingspot.com/how-to-resolve-net-reference-and-nuget-package-version-conflicts/)
- [Understanding How Assemblies Load in C# .NET](https://michaelscodingspot.com/assemblies-load-in-dotnet/)
- [Serilog Do's and Don'ts](https://esg.dev/posts/serilog-dos-and-donts/)
- [The future of .NET Standard](https://devblogs.microsoft.com/dotnet/the-future-of-net-standard/)
- [Introducing C# Source Generators](https://devblogs.microsoft.com/dotnet/introducing-c-source-generators/)
- [Performance Improvements in .NET 5](https://devblogs.microsoft.com/dotnet/performance-improvements-in-net-5/)
- [Announcing .NET 5.0](https://devblogs.microsoft.com/dotnet/announcing-net-5-0/)
- [Optimising .NET Core Docker images](https://benfoster.io/blog/optimising-dotnet-docker-images/)
- [Serilog Best Practices](https://benfoster.io/blog/serilog-best-practices/)
- [Creating Windows Services In .NET Core – Part 1 – The “Microsoft” Way](https://dotnetcoretutorials.com/2019/09/19/creating-windows-services-in-net-core-part-1-the-microsoft-way/) + part 2, part 3
- [Eliding Async and Await](https://blog.stephencleary.com/2016/12/eliding-async-await.html)
- [Pipelines | Microsoft Docs](https://docs.microsoft.com/en-us/previous-versions/msp-n-p/ff963548(v=pandp.10)?redirectedfrom=MSDN)
- [Welcome to C# 9.0 | .NET Blog](https://devblogs.microsoft.com/dotnet/welcome-to-c-9-0/)
- [ASP.NET Core Authentication with IdentityServer4 | ASP.NET Blog](https://devblogs.microsoft.com/aspnet/asp-net-core-authentication-with-identityserver4/)
- [Bearer Token Authentication in ASP.NET Core | ASP.NET Blog](https://devblogs.microsoft.com/aspnet/bearer-token-authentication-in-asp-net-core/)
- [ConfigureAwait FAQ | .NET Blog](https://devblogs.microsoft.com/dotnet/configureawait-faq/)
- [Building high performance database queries using Entity Framework Core and AutoMapper – The Reformed Programmer](https://www.thereformedprogrammer.net/building-efficient-database-queries-using-entity-framework-core-and-automapper/?fbclid=IwAR0ryB01m13Bb2Wr2ToOkIEQUx_IF1qt8btTlY7wRzLK0PNJwxZERZtSpPI)
- [Must Windsor track my components?](https://kozmic.net/2010/08/19/must-windsor-track-my-components/)
- [Must I release everything when using Windsor?](https://kozmic.net/2010/08/27/must-i-release-everything-when-using-windsor/)

# Tools
- [.NET Core Source Browser - source.dot.net](https://source.dot.net/)
- [The .NET Portability Analyzer](https://docs.microsoft.com/en-us/dotnet/standard/analyzers/portability-analyzer)

# Olvasnivaló
- [An Extensive Examination of Data Structures Using C# 2.0](https://docs.microsoft.com/en-us/previous-versions/ms379570%28v%3dvs.80%29)
  - [x] [Part 1](https://docs.microsoft.com/en-us/previous-versions/ms379570%28v%3dvs.80%29) - An Introduction to Data Structures
  - [x] [Part 2](https://docs.microsoft.com/en-us/previous-versions/ms379571%28v%3dvs.80%29) - The Queue, Stack and Hashtable
  - [x] [Part 3](https://docs.microsoft.com/en-us/previous-versions/ms379572%28v%3dvs.80%29) - Binary Trees and BSTs
  - [ ] [Part 4](https://docs.microsoft.com/en-us/previous-versions/ms379573(v=vs.80)) - Building a Better Binary Search Tree
  - [ ] [Part 5](https://docs.microsoft.com/en-us/previous-versions/ms379574(v%3dvs.80)) - From Trees to Graphs
  - [ ] [Part 6](https://docs.microsoft.com/en-us/previous-versions/ms379575(v%3dvs.80)) - Efficiently Representing Sets

# C#
- [C# 9.0 on the record](https://devblogs.microsoft.com/dotnet/c-9-0-on-the-record/)
- [github.com/dotnet/csharplang](https://github.com/dotnet/csharplang/tree/master/spec)

# WCF in .NET Core
- [Using WCF With .NET Core - The Seeley Coder](https://www.seeleycoder.com/blog/using-wcf-with-dotnetcore/)
- [Migrating WCF to gRPC using .NET Core - The Seeley Coder](https://www.seeleycoder.com/blog/migrating-wcf-to-grpc-netcore/)
