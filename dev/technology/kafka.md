## General
- [Scalability of Kafka Messaging using Consumer Groups](https://blog.cloudera.com/scalability-of-kafka-messaging-using-consumer-groups/)
- [My Python/Java/Spring/Go/Whatever Client Won’t Connect to My Apache Kafka Cluster in Docker/AWS/My Brother’s Laptop. Please Help!](https://www.confluent.io/blog/kafka-client-cannot-connect-to-broker-on-aws-on-docker-etc/)
- [Confluent Platform VS Apache Kafka: What are the differences?](https://www.cesaro.io/post/confluent-platform-vs-apache-kafka-what-are-the-differences)
- [Kafka Log Retention and Cleanup Policies](https://medium.com/@sunny_81705/kafka-log-retention-and-cleanup-policies-c8d9cb7e09f8)
- [**Benchmarking Apache Kafka**: 2 Million Writes Per Second (On Three Cheap Machines)](https://engineering.linkedin.com/kafka/benchmarking-apache-kafka-2-million-writes-second-three-cheap-machines) - 2014.04
- [Kafka replication](https://www.slideshare.net/junrao/kafka-replication-apachecon2013) - és jó átfogó kép a kafkáról pár szempontból

## .NET client
- [Confluent.Kafka](https://docs.confluent.io/current/clients/dotnet.html)
- [confluentinc/confluent-kafka-dotnet GitHub repo](https://github.com/confluentinc/confluent-kafka-dotnet)

## Confluent blog
- [Why Avro for Kafka Data?](https://www.confluent.io/blog/avro-kafka-data/) - 2015.02
- [How to choose the number of topics/partitions in a Kafka cluster?](https://www.confluent.io/blog/how-choose-number-topics-partitions-kafka-cluster/) - 2015.03
- [Decoupling Systems with Apache Kafka, Schema Registry and Avro](https://www.confluent.io/blog/decoupling-systems-with-apache-kafka-schema-registry-and-avro/) - 2018.08
- [Kafka Listeners – Explained](https://www.confluent.io/blog/kafka-listeners-explained/) - 2019.07
- [Apache Kafka Producer Improvements with the Sticky Partitioner](https://www.confluent.io/blog/apache-kafka-producer-improvements-sticky-partitioner/) - 2019.12

## Tools
- [edenhill/kafkacat](https://github.com/edenhill/kafkacat)

## Examples
- [confluentinc/demo-scene/build-a-streaming-pipeline](https://github.com/confluentinc/demo-scene/tree/master/build-a-streaming-pipeline)
