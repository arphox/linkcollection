## Intro (OOP vs FP)
- [Object-Oriented vs. Functional Programming With C# and F#](https://www.youtube.com/watch?v=vOcTPUiBQgc)

## F#
- [F# Software Foundation](https://fsharp.org/)
- [F# for Fun and Profit](https://fsharpforfunandprofit.com/site-contents/)

## Books
- [Get Programming with F#: A guide for .NET developers](https://www.amazon.com/Get-Programming-guide-NET-developers/dp/1617293997)
- [F# in Action](https://www.manning.com/books/f-sharp-in-action)
- [Domain Modeling Made Functional](https://www.amazon.com/Domain-Modeling-Made-Functional-Domain-Driven/dp/1680502549): Tackle Software Complexity with Domain-Driven Design and F#
