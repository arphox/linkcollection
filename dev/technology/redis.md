# Redis
- [redis.io](https://redis.io/), [try-redis](https://try.redis.io/)
- [redis google group](https://groups.google.com/forum/#!forum/redis-db)
- [/r/redis](https://www.reddit.com/r/redis/)
- [github: redis/redis](https://github.com/redis/redis)

## Redis with C#
- [StackExchange/StackExchange.Redis](https://github.com/StackExchange/StackExchange.Redis) - good and free C# client

## Articles
- [Running Redis on Windows 10 – Part I of III](https://redislabs.com/blog/redis-on-windows-10/)
