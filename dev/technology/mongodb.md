# MongoDB blog
- [Quick Start: C# and MongoDB - Starting and Setup](https://www.mongodb.com/blog/post/quick-start-c-sharp-and-mongodb-starting-and-setup) - 2019.09

## C#
- [Mongo C# driver Quick Tour](https://mongodb.github.io/mongo-csharp-driver/2.11/getting_started/quick_tour/)
- [Mapping classes](https://mongodb.github.io/mongo-csharp-driver/2.11/reference/bson/mapping/)
