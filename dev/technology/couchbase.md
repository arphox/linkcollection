# Videos
- [What is Couchbase?](https://www.youtube.com/watch?v=uBIJBmc9FWA)
- [Why Couchbase?](https://www.youtube.com/watch?v=o9XIzmfZNow)
- [Learn Couchbase In 1 Hour - Complete Couchbase Tutorial - Couchbase Tutorial For Beginners](https://www.youtube.com/watch?v=pV65gLFf0tI)

# Courses
- [Developer Portal | Couchbase](https://developer.couchbase.com/) - official courses for free, I enrolled but never completed.
