## Learning links
- [Azure Cosmos DB documentation](https://docs.microsoft.com/en-us/azure/cosmos-db/) on microsoft docs
- [AzureCosmosDB - YouTube](https://www.youtube.com/c/AzureCosmosDB/videos)
- [Microsoft Learn - CosmosDb collection-öm](https://docs.microsoft.com/en-us/users/arphox/collections/kogrtneknexr4z) - ide válogatom be a cosmos-os dolgokat.

#### Completed
- [AZ-204: Develop solutions that use Azure Cosmos DB](https://docs.microsoft.com/en-us/learn/paths/az-204-develop-solutions-that-use-azure-cosmos-db/) path
- [Azure Cosmos DB Essentials Series - YouTube](https://www.youtube.com/playlist?list=PLLasX02E8BPDd2fKwLCHnmWoyo4bL-oKr) - season 2 végéig megnéztem
- [PluralSight - Data Literacy: Essentials of Azure Cosmos DB](https://app.pluralsight.com/library/courses/data-literacy-essentials-azure-cosmosdb/table-of-contents)
- [PluralSight - Microsoft Azure Developer: Develop Solutions with Cosmos DB Storage](https://app.pluralsight.com/library/courses/microsoft-azure-developer-develop-solutions-cosmos-db-storage/table-of-contents)
- [PluralSight - Learning Azure Cosmos DB](https://app.pluralsight.com/library/courses/azure-cosmos-db/table-of-contents)

#### Certs
- [Microsoft Certified: Azure Cosmos DB Developer Specialty](https://docs.microsoft.com/en-us/certifications/azure-cosmos-db-developer-specialty/)

#### Articles
- [How to efficiently write millions of records in the cloud and not go bankrupt — an Azure CosmosDB case study · allegro.tech](https://blog.allegro.tech/2022/09/azure-cosmosdb-case-study.html)
