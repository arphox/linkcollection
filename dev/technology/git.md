_Insert new lines at the BEGINNING._

## Books
- [Pro Git](https://git-scm.com/book/): official, free Git book

## Conventions
- [No Reason To Squash](https://arialdomartini.github.io/no-reason-to-squash)
- [A tidy, linear Git history](https://www.bitsnbites.eu/a-tidy-linear-git-history/)
- [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/#separate)

## Articles
- [PullRequest (Martin Fowler)](https://martinfowler.com/bliki/PullRequest.html)
- [Patterns for Managing Source Code Branches (Martin Fowler)](https://martinfowler.com/articles/branching-patterns.html)