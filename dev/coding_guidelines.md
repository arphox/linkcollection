## General

### Functions/Methods
- Always call the return value from a function `result`. (Martin Fowler, Refactoring 2, p. 9)

## Habits / practices
These are not strong rules, but more like habits or practices you would usually see in a C# codebase.

- Prefer private fields over private properties in general (e.g. Nick Chapsas' video: ["Don't Use Fields in C#! Use Properties Instead" | Code Cop #003](https://www.youtube.com/watch?v=QdfAOVk77v0)), but _sometimes_ they can be useful ([Eric Lippert](https://stackoverflow.com/a/3310469/4215389)).
