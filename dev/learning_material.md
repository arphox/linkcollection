## Oktatóoldalak
- [freeCodeCamp](https://learn.freecodecamp.org/)
- [Coursera](https://www.coursera.org/)
- [MIT OpenCourseWare](https://ocw.mit.edu/index.htm)
- [Stanford Online](https://online.stanford.edu/)
- [edX](https://www.edx.org/)
  - [CS50](https://www.edx.org/cs50) - CS50 Computer Science Courses from Harvard
    - [CS50's AP(R) Computer Science Principles](https://courses.edx.org/courses/course-v1:HarvardX+CS50+AP/course/)
- [CodeAcademy](https://www.codecademy.com/) - Learn to Code - for Free
- [The Odin Project](https://www.theodinproject.com/)

### Nagyrészt fizetős oldalak
#### Általános
- [SkillShare](https://www.skillshare.com/) - subscription-based, havi 32? USD. Mindenféle témában.
- [Brilliant.org](https://brilliant.org/) - subscription alapú, havi~12 USD. Témák:  Foundational math, Advanced math, Science, Computer science
- [MasterClass](https://www.masterclass.com/) - subscription-alapú (csak éves van), havi ~19 USD. Témák: writing, cooking, sports, business, wellness, and more
#### IT & Business
- [PluralSight](https://www.pluralsight.com/) - subscription-based, havi 29 USD.
- [Udemy](https://www.udemy.com/) - per course pricing
  - [100% Off Udemy Coupons: Last Minute Udemy Codes](https://www.onlinecourses.ooo/)
- [Udacity](https://www.udacity.com/) - per course pricing, ez a legdrágább. Vannak "nanodegree"-k: pár hónap alatt egy szakterületet elsajátítani. Kb. havi 400 USD, annyi mint a PluralSight éves előfizetése.

## Egyéb
- [Developer Roadmaps](https://roadmap.sh/)

### Read-only
- [SourceMaking.com](https://sourcemaking.com/) - Design Patterns & Refactoring
- [Refactoring.guru](https://refactoring.guru/) - Refactoring.Guru makes it easy for you to discover everything you need to know about refactoring, design patterns, SOLID principles and other smart programming topics.

## Videó
- [**GOTO Conferences**](https://www.youtube.com/c/GotoConferences/videos)
- [**CraftHub Events**](https://www.youtube.com/channel/UC9E-wqsOP_1nRKXWIWPBhXw/videos) - **CRAFT** és egyéb videók
- [**NDC Conferences**](https://www.youtube.com/c/NDCConferences/videos)
- [**Software Craftsmanship London**](https://www.youtube.com/c/Codurance/playlists)
- [**Øredev Conference**](https://vimeo.com/oredev)
  - [2019](https://vimeo.com/search?q=%C3%98redev+2019) - canceled in 2020 & 2021
  - [2018](https://vimeo.com/search?q=%C3%98redev+2018)
  - [2017](https://vimeo.com/search?q=%C3%98redev+2017)
  - [2016](https://vimeo.com/search?q=%C3%98redev+2016)
  - [2015](https://vimeo.com/search?q=%C3%98redev+2015)
  - [2014](https://vimeo.com/search?q=%C3%98redev+2014)
  - [2013](https://vimeo.com/search?q=%C3%98redev+2013)
  - [2011](https://vimeo.com/search?q=%C3%98redev+2011)
  - [2010](https://vimeo.com/search?q=%C3%98redev+2010)
  - [2009](https://vimeo.com/search?q=%C3%98redev+2009)
- [**DevTernity Conference**](https://www.youtube.com/c/DevTernity/featured)
- [freeCodeCamp.org](https://www.youtube.com/channel/UC8butISFwT-Wl7EV0hUK0BQ/videos)
- [Coding Tech](https://www.youtube.com/channel/UCtxCXg-UvSnTKPOzLH4wJaQ/videos)
- [Jamie King](https://www.youtube.com/user/1kingja/videos) - not active anymore but has a lot of good videos

## Hang (~podcast)
- **[awesome](https://github.com/sindresorhus/awesome) - Awesome lists about all kinds of interesting topics**
- [awesome-podcasts](https://github.com/rShetty/awesome-podcasts) - Awesome list of Important Podcasts for software engineers

## Documentation
- [Microservices with .NET](https://dotnet.microsoft.com/apps/aspnet/microservices) - TODO read!
- [.NET Documentation (docs.microsoft.com)](https://docs.microsoft.com/en-us/dotnet/)

## Egyetemek
- [InfoC](https://infoc.eet.bme.hu/) - sok érdekesség, végig kéne egyszer olvasni
- [Funkcionális programozás jegyzet (Haskell)](http://lambda.inf.elte.hu/Index.xml)

## Emberek
- [Donald Knuth](https://www-cs-faculty.stanford.edu/~knuth/)

## Archívum
- [Laws of UX](https://lawsofux.com/)

## Témák szerint
### Git
- [Learn Git Branching](https://learngitbranching.js.org/) - kiváló, játékos interaktív Git oktató. Főleg csak a branching-gel kapcsolatos dolgok vannak benne: commit, merge, rebase, fetch, pull, push, stb.
- [Git - About (official)](https://git-scm.com/about) - nagyjából leírja, hogy mit tud feature szinten a git
- [Git - Documentation (official)](https://git-scm.com/doc)
  - [Videos](https://git-scm.com/videos)
  - [GitHub Git cheat sheet](https://github.github.com/training-kit/downloads/github-git-cheat-sheet.pdf) - jó, rövid cheat sheet a GitHubtól
  - [Visual git cheat sheet](https://ndpsoftware.com/git-cheatsheet.html) - vizuális cheat sheet a gyakran használt parancsokról
    - [Escape a git mess](http://justinhileman.info/article/git-pretty/git-pretty.png)
