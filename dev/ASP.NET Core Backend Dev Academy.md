#### Middleware in ASP.NET 6 series
- [Middleware in ASP.NET 6 - Intro and Basics](https://exceptionnotfound.net/middleware-in-asp-dotnet-6-intro-and-basics/)
- [Middleware in ASP.NET 6 - Custom Middleware Classes](https://exceptionnotfound.net/middleware-in-asp-net-6-custom-middleware-classes/)
- [Middleware in ASP.NET 6 - Order of Operations](https://exceptionnotfound.net/middleware-in-dotnet-6-order-of-operations/)
- [Middleware in ASP.NET 6 - Conditionally Adding Middleware to the Pipeline](https://exceptionnotfound.net/middleware-in-dotnet-6-conditionally-adding-middleware-to-the-pipeline/)

# General topics
- [Clean Code - Uncle Bob - all lessons - YouTube](https://www.youtube.com/playlist?list=PLmmYSbUCWJ4x1GO839azG_BBw8rkh-zOj)

# Logging
- [Logging with ILogger in .NET: Recommendations and best practices - Rico Suter's blog.](https://blog.rsuter.com/logging-with-ilogger-recommendations-and-best-practices/)

# ASP.NET Core
- [**davidfowl/AspNetCoreDiagnosticScenarios**](https://github.com/davidfowl/AspNetCoreDiagnosticScenarios) - VERY useful stuff!
