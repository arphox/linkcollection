1. **Boy scout rule**: Always leave the code better than you found it. (Also known as "camping rule")
1. **YAGNI**: You Aren't Gonna Need It
1. **DRY**: Don't Repeat Yourself
1. **[KISS](https://en.wikipedia.org/wiki/KISS_principle)**: Keep It Simple, Stupid
1. Optimise code for readability

## Sentences
- "You are not done when it work. You are done when it's right." (Uncle Bob)
-  ["The only way to go fast is to go well."](https://www.goodreads.com/quotes/9701819-the-only-way-to-go-fast-is-to-go-well) (Uncle Bob?)
- ["For each desired change, make the change easy (warning: this may be hard), then make the easy change"](https://twitter.com/kentbeck/status/250733358307500032) - Kent Beck
- "You spend more time reading code than writing it." → Optimise code for readability (Mark Seemann, Code That Fits Your Head, ch. 3.2.2)
  - "Any fool can write code that a computer can understand. Good programmers write code that humans can understand" (Martin Fowler)

## Advices
- Treat compiler warnings, linter and static code analysis warnings as errors. (Mark Seemann, Code That Fits Your Head, ch. 2.2.3)

## Laws
- [Hyrum's Law](https://www.hyrumslaw.com/)