# General
- [On Testing: Why write tests?](https://kozmic.net/2013/01/19/on-testing-why-write-tests/)
- [Test Behavior, Not Implementation](https://testing.googleblog.com/2013/08/testing-on-toilet-test-behavior-not.html) (Google Testing Blog, 2013)

## Fluent builders
- [Test Data Builders: an alternative to the Object Mother pattern](http://www.natpryce.com/articles/000714.html)
  - [Tricks with Test Data Builders: Defining Common State](http://www.natpryce.com/articles/000724.html)
  - [Tricks with Test Data Builders: Combining Builders](http://www.natpryce.com/articles/000726.html)
  - [Tricks with Test Data Builders: Emphase the Domain Model with Factory Methods](http://www.natpryce.com/articles/000727.html)
  - [Tricks with Test Data Builders: Refactoring Away Duplicated Logic Creates a Domain Specific Embedded Language for Testing](http://www.natpryce.com/articles/000728.html)
- [A Fluent Builder in C#](https://arialdomartini.github.io/fluent-builder.html)
  - [Using domain language in C# Fluent Builders](https://arialdomartini.github.io/using-domain-language-in-fluent-builders)

# Unit test
- [bliki/UnitTest](https://martinfowler.com/bliki/UnitTest.html) - Sociable vs Solitary, etc

# Integration test
- [When I'm done, I don't clean up](https://arialdomartini.github.io/when-im-done-i-dont-clean-up)

# xUnit
- [Why Did we Build xUnit 1.0?](https://xunit.net/docs/why-did-we-build-xunit-1.0.html)
- [NET Core 2: Why xUnit and not NUnit or MSTest](https://dev.to/hatsrumandcode/net-core-2-why-xunit-and-not-nunit-or-mstest--aei)

# Infographics
- [Unit Testing Principles, Practices, and Patterns by. Vladimir Khorikov](https://khorikov.org/files/infographic.pdf) - probably an infographic excerpt of the book

# Other
## nUnit
- [Why you should not use SetUp and TearDown in NUnit](https://jamesnewkirk.typepad.com/posts/2007/09/why-you-should-.html)
