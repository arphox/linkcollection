# Problem solving sites

## TODO -> TRY THEM!
- [CodeForces](http://codeforces.com/) - recommended by betaveros
- [31 Coders Games and Puzzle Sites](https://www.reddit.com/r/learnprogramming/comments/43upct/31_coders_games_and_puzzle_sites/)
- [TopCoder](https://www.topcoder.com/challenges)
- [CodeChef](https://www.codechef.com/)
- [CodeSignal](https://codesignal.com/developers/)
- [CoderByte](https://www.coderbyte.com/)
- [CodinGame](https://www.codingame.com/start)
- [GeeksForGeeks](https://practice.geeksforgeeks.org/)
- [LeetCode](https://leetcode.com/)
- [Project Euler](https://projecteuler.net/)
- [SPOJ - Sphere Online Judge](https://www.spoj.com/)

## Standard
- [CodeWars](https://www.codewars.com/)
- [HackerRank](https://www.hackerrank.com/)

## Non-conventional
- [Advent of Code](https://adventofcode.com/)
  - [reddit](https://www.reddit.com/r/adventofcode/)

## Non-competitive
 - [CodingBat](https://codingbat.com) - CodingBat is a free site of live coding problems to build coding skill in Java and Python (example problem). CodingBat is a project by Nick Parlante, a computer science lecturer at Stanford.

## Unique
- [Elevator Saga - The elevator programming game](https://play.elevatorsaga.com/) - Gotta code in JavaScript
- [The Deadlock Empire](https://deadlockempire.github.io/) - challenges in concurrency topic

## Katas (Coding Dojo)
- [Kata-Log](http://kata-log.rocks/)
