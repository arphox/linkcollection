#### 2025-W11
- [AWS Step Functions Introduction - What is it and Why is it Useful? - YouTube](https://www.youtube.com/watch?v=QXh2WF1EJvs)

#### 2025-W09
- [Against best practices](https://www.arp242.net/best-practices.html)
- [Application publishing - .NET | Microsoft Learn](https://learn.microsoft.com/en-us/dotnet/core/deploying/)
- [How Good Are Self-Contained Applications in .NET 9?](https://blog.inedo.com/dotnet/self-contained-applications/)
- [Implementing Canary Deployments of AWS Lambda Functions with Alias Traffic Shifting | AWS Compute Blog](https://aws.amazon.com/blogs/compute/implementing-canary-deployments-of-aws-lambda-functions-with-alias-traffic-shifting/)

#### 2025-W07
- [The Practical Test Pyramid](https://martinfowler.com/articles/practical-test-pyramid.html)

#### 2025-W04
- [Orchestration vs. Choreography: The good, the bad, and the trade-offs - Laila Bougria - NDC Porto - YouTube](https://www.youtube.com/watch?v=p8NTe7NFhH8)

#### 2024-W52
- [That's not an abstraction, that's just a layer of indirection](https://fhur.me/posts/2024/thats-not-an-abstraction)

#### 2024-W50
- **FINISHED A BOOK! 🎉🚀** – [Code That Fits in Your Head : Heuristics for Software Engineering](https://www.amazon.com/Code-That-Fits-Your-Head/dp/0137464401) by Mark Seemann
- [Software engineers suffer from Dunning-Kruger - do you too?](https://dateo-software.de/blog/dunning-kruger)

#### 2024-W49
- [What is Reliability Engineering? - by Gergely Orosz](https://newsletter.pragmaticengineer.com/p/reliability-engineering?utm_source=substack&utm_medium=email) (free part)
- [When You Shouldn't Use Await Async in .NET - YouTube](https://www.youtube.com/watch?v=ktSlDGdZRMs)
- [Clean Architecture with ASP.NET Core 9 - YouTube](https://www.youtube.com/watch?v=zw-ZtB1BNl8)

#### 2024-W48
- [What's new in the .NET Runtime, Libraries, & SDK - YouTube](https://www.youtube.com/watch?v=4iEqqPZKDC0)
- Participated on a **full-day AWS EKS Workshop** about containerization in AWS, EKS, workspace, clusters, security best practices, auth.
- [dotnet CLI all the things! - YouTube](https://www.youtube.com/watch?v=r1TBZeVgJmU)
- [C#'s Best features you might not be using - YouTube](https://www.youtube.com/watch?v=yuXw7oj0Bg0)
- [Stop Using AutoMapper in .NET - YouTube](https://www.youtube.com/watch?v=RsnEZdc3MrE)
- [The Best .NET Mapper to Use in 2023 - YouTube](https://www.youtube.com/watch?v=U8gSdQN2jWI)

#### 2024-W47
- [Welcome to .NET 9 - .NET Conf 2024 Keynote - YouTube](https://www.youtube.com/watch?v=ikSNL-lxolc)

#### 2024-W46
- [Configure Lambda function memory - AWS Lambda](https://docs.aws.amazon.com/lambda/latest/dg/configuration-memory.html)
- [Understanding Lambda function scaling - AWS Lambda](https://docs.aws.amazon.com/lambda/latest/dg/lambda-concurrency.html)
- [Building Lambda functions with C# - AWS Lambda](https://docs.aws.amazon.com/lambda/latest/dg/lambda-csharp.html)
- [Define Lambda function handler in C# - AWS Lambda](https://docs.aws.amazon.com/lambda/latest/dg/csharp-handler.html)
- [Build and deploy C# Lambda functions with .zip file archives - AWS Lambda](https://docs.aws.amazon.com/lambda/latest/dg/csharp-package.html)
- [AWS Lambda function testing in C# - AWS Lambda](https://docs.aws.amazon.com/lambda/latest/dg/dotnet-csharp-testing.html)
- [You Can Test & Debug Your .NET Lambda Functions Locally - YouTube](https://www.youtube.com/watch?v=962ba6mgQXI)
- [Understand the Lambda execution environment lifecycle - AWS Lambda](https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtime-environment.html#runtimes-lifecycle-ib)
- [Using a Memory Cache with .NET Lambda Functions | no dogma blog](https://nodogmablog.bryanhogan.net/2022/07/using-a-memory-cache-with-net-lambda-functions/)
- [Using a Distributed Memory Cache with .NET Lambda Functions | no dogma blog](https://nodogmablog.bryanhogan.net/2022/10/using-a-distributed-memory-cache-with-net-lambda-functions/)
- [Lambda cold starts for .NET applications are not so bad | no dogma blog](https://nodogmablog.bryanhogan.net/2023/02/lambda-cold-starts-for-net-applications-are-not-so-bad/)
- [Getting the JSON sent to a Lambda Function when Deserialization Fails | no dogma blog](https://nodogmablog.bryanhogan.net/2023/02/getting-the-json-sent-to-a-lambda-function-when-deserialization-fails/)


#### 2024-W45
- [Introduction to AWS Lambda - Serverless Compute on Amazon Web Services - YouTube](https://www.youtube.com/watch?v=eOBq__h4OJ4)
- [Serverless was a big mistake... says Amazon - YouTube](https://www.youtube.com/watch?v=qQk94CjRvIs)
- [Introduction to Amazon Elastic Container Service (ECS) | Amazon Web Services - YouTube](https://www.youtube.com/watch?v=FnFvpIsBrog)
- [An Overview of AWS Elastic Container Service (ECS) - YouTube](https://www.youtube.com/watch?v=I9VAMGEjW-Q)
- [AWS EC2 vs ECS vs Lambda | Which is right for YOU? - YouTube](https://www.youtube.com/watch?v=-L6g9J9_zB8)
- [Back to Basics: Deploying Code to ECS - YouTube](https://www.youtube.com/watch?v=lC_fxY9uBrw)
- [What is Amazon Elastic Container Service? - Amazon Elastic Container Service](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/Welcome.html)

#### 2024-W44
- [The Best Engineer I worked with at Google - YouTube](https://www.youtube.com/watch?v=ugm09VUhM5E): 100% focused (no distractions), proactive, perfect work-life balance (~35h/week)
- [Microservices are Technical Debt - YouTube](https://www.youtube.com/watch?v=LcJKxPXYudE)

#### 2024-W38
- [You can side-step a yak, they don't all need to be shaved | Swizec Teller](https://swizec.com/blog/you-can-side-step-a-yak-they-dont-all-need-to-be-shaved/)
- [How big up-front design fails | Swizec Teller](https://swizec.com/blog/how-big-up-front-design-fails/)

#### 2024-W37
- [Continuous reinvention: A brief history of block storage at AWS | All Things Distributed](https://www.allthingsdistributed.com/2024/08/continuous-reinvention-a-brief-history-of-block-storage-at-aws.html)
- [How to be the best programmer, according to Daniel Terhorst-North - ShiftMag](https://shiftmag.dev/the-best-programmer-daniel-terhorst-north-3526/)

#### 2024-W32
- [Is High Quality Software Worth the Cost?](https://martinfowler.com/articles/is-quality-worth-cost.html) (Martin Fowler)
- [You must not measure individual software engineer performance | by Jack Danger | Dangerous Engineering | Medium](https://medium.com/dangerous-engineering/you-must-not-measure-individual-software-engineer-performance-3b68cc45cacb)
- [AWS Cloud Practitioner Essentials](https://explore.skillbuilder.aws/learn/course/134/aws-cloud-practitioner-essentials): Modules 8-11.

#### 2024-W31
- [Anyone can Access Deleted and Private Repository Data on GitHub ◆ Truffle Security Co.](https://trufflesecurity.com/blog/anyone-can-access-deleted-and-private-repo-data-github)

#### 2024-W30
- [AI Tooling for Software Engineers in 2024: Reality Check (Part 1)](https://newsletter.pragmaticengineer.com/p/ai-tooling-2024) (full article)

#### 2024-W29
- [On Pair Programming](https://martinfowler.com/articles/on-pair-programming.html) (Martin Fowler)
- [It's probably time to stop recommending Clean Code @ Things Of Interest](https://qntm.org/clean)
- [Clean your codebase with basic information theory](https://taylor.town/compress-code)
- [Inside OpenAI: How does ChatGPT Ship So Quickly?](https://newsletter.pragmaticengineer.com/p/inside-openai-how-does-chatgpt-ship) (free part)
- [The Past and Future of Modern Backend Practices](https://newsletter.pragmaticengineer.com/p/the-past-and-future-of-backend-practices) (free part)
- [The Right Kind of Stubborn](https://paulgraham.com/persistence.html): The five qualities to be persistent: energy, imagination, resilience, good judgement, focus on a goal.
- [Use A Work Journal To Recover Focus Faster And Clarify Your Thoughts](https://fev.al/posts/work-journal/)

#### 2024-W28
- [AWS Cloud Practitioner Essentials](https://explore.skillbuilder.aws/learn/course/134/aws-cloud-practitioner-essentials): Module 7.
- [Run your app from a ZIP package - Azure App Service | Microsoft Learn](https://learn.microsoft.com/en-us/azure/app-service/deploy-run-package)
- [What is Old is New Again - by Gergely Orosz](https://newsletter.pragmaticengineer.com/p/what-is-old-is-new-again)

#### 2024-W27
- [Small Functions considered Harmful | by Cindy Sridharan | Medium](https://copyconstruct.medium.com/small-functions-considered-harmful-91035d316c29)
- [Linear code is more readable](https://blog.separateconcerns.com/2023-09-11-linear-code.html)
- [Instead of "auth", we should say "permissions" and "login" | nicole@web](https://ntietz.com/blog/lets-say-instead-of-auth/)
- [Analyzing my electricity consumption | zdimension](https://zdimension.fr/analyzing-my-electricity-consumption/)
- [Maciej Walkowiak | PostgreSQL and UUID as primary key](https://maciejwalkowiak.com/blog/postgres-uuid-primary-key/)
- [Trimodal Nature of Tech Compensation Revisited](https://newsletter.pragmaticengineer.com/p/trimodal-nature-of-tech-compensation)
- [Intel Management Engine - Wikipedia](https://en.m.wikipedia.org/wiki/Intel_Management_Engine)
- [Tanenbaum–Torvalds debate - Wikipedia](https://en.wikipedia.org/wiki/Tanenbaum%E2%80%93Torvalds_debate)
- [zakirullin/cognitive-load: 🧠 Cognitive Load is what matters](https://github.com/zakirullin/cognitive-load)
- [Senior Engineer Fatigue - Blog | luminousmen](https://luminousmen.com/post/senior-engineer-fatigue)
- [So We've Got a Memory Leak… | Steven Harman — Maker & Breaker of Things](https://stevenharman.net/so-we-have-a-memory-leak)
- [Dotnet Core - Filter out specific test projects when running dotnet test](https://josef.codes/dotnet-core-filter-out-specific-test-projects-when-running-dotnet-test/)
- [AWS Cloud Practitioner Essentials](https://explore.skillbuilder.aws/learn/course/134/aws-cloud-practitioner-essentials): Module 6.

#### 2024-W26
- [AWS Cloud Practitioner Essentials](https://explore.skillbuilder.aws/learn/course/134/aws-cloud-practitioner-essentials): Modules 1-5. 
- [Three Laws of Software Complexity (or: why software engineers are always grumpy) | mahesh’s blog](https://maheshba.bitbucket.io/blog/2024/05/08/2024-ThreeLaws.html): A well-designed system will degrade into a badly designed system over time. Complexity is a Moat (filled by Leaky Abstractions), most successful/popular systems are badly designed systems. There is no fundamental upper limit on Software Complexity.
- [Decoding Career Levels in Software Engineering! | by Aravind | Medium](https://medium.com/@aravind16101800/decoding-career-levels-in-software-engineering-f06b7e763da5)
- [Designations, levels and calibrations. | Irrational Exuberance](https://lethain.com/perf-management-system/)
- [Staff archetypes | StaffEng](https://staffeng.com/guides/staff-archetypes/): Tech Lead, Architect, Solver, Right Hand.
- [Build vs Buy: The 6 steps framework to avoid disasters](https://divbyzero.com/blog/build-vs-buy/): "How core is this?". "Does it provide competitive advantage?". Prefer simplicity! "Which is the fastest go-live way?". "Do I have competency to build it?". Compare costs.
- [Sidecar Proxy Pattern - The Basis Of Service Mesh](https://iximiuz.com/en/posts/service-proxy-pod-sidecar-oh-my/): sidecar proxy pattern (e.g. envoy) helps offloading repeated logic (e.g. SSL termination & authentication for incoming requests; retry, timeout, tracing for outgoing) from services in a microservice architecture. Service mesh, pod (=group of containers), podman, prometheus.
- [How I built a huge graph database of Netflix's cloud infrastructure](https://blog.dataengineer.io/p/how-i-built-a-huge-graph-database)
- [From Liberal Arts Major To Amazon Principal Engineer: Steve Huynh](https://www.developing.dev/p/from-liberal-arts-major-to-amazon): putting in more hours at the right times can make a big difference in career progression, but doing so all of the time will lead to burnout.
- [Thoughts on Code Reviews 🔍 - by Luca Rossi](https://hybridhacker.email/p/thoughts-on-code-reviews): no/slow/superficial review = bad. Use coding conventions and static code analysis (shorter feedback loop). IF YOU ARE MATURE ENOUGH, Use different review process for different changes (simple=ship/show, complex=ask). Async+Blocking+Mandatory → Automate+Defer+Pair.
- [Communicate like a Senior: Phrases used by the best leaders](https://read.highgrowthengineer.com/p/communicate-like-a-senior-phrases): "I noticed...", get agreement on the problem before jumping to the solution, "Thank you for...",  "What do you think?", "I agree", 
- [The Documentation Tradeoff - by Kent Beck](https://tidyfirst.substack.com/p/the-documentation-tradeoff): Focus on communication either with code, tests, direct interaction or documentation. Everything is a tradeoff, skipping documentation included.
- [LUC #57: Essential Caching Strategies for Optimal Performance](https://blog.levelupcoding.com/p/luc-57-essential-caching-strategies-optimal-performance): LRU, MRU, LFU, TTL, Multi-tiered, FIFO, RR, ARC. REST vs GraphQL vs SOAP vs gRPC vs WebSockets vs MQTT. Circuit breaker.
- [How I Mastered Data Structures and Algorithms](https://blog.algomaster.io/p/how-i-mastered-data-structures-and-algorithms): Have a plan, solve real-world problems, repetition, understand instead of memorizing.
- [Redis Use Cases - by Neo Kim - System Design Newsletter](https://newsletter.systemdesign.one/p/redis-use-cases)
- [Redis Adopts Dual Source-Available Licensing - Redis](https://redis.io/blog/redis-adopts-dual-source-available-licensing/)
- [Elided Branches: Structural Lessons in Engineering Management](https://www.elidedbranches.com/2022/01/structural-lessons-in-engineering.html)
- [Things DBs Don't Do - But Should](https://www.thenile.dev/blog/things-dbs-dont-do?utm_source=blog.quastor.org&utm_medium=referral&utm_campaign=how-linkedin-reduced-latency-with-json): schema version control, tenant-awareness, change events, soft delete, composable APIs, modern protocols, global db, intelligent & adaptive db.
- [How SMS Fraud Works and How to Guard Against It](https://technicallythinking.substack.com/p/how-sms-fraud-works-and-how-to-guard-against-it): Premium phone numbers cost money to receive SMS -> attackers can use this to steal money from you.
- [How LinkedIn Reduced Latency with JSON](https://blog.quastor.org/p/linkedin-reduced-latency-json-3797?utm_source=www.hungryminds.dev&utm_medium=newsletter&utm_campaign=linkedin-latency-101-1-trick-to-serve-over-1b-users): JSON to Protobuf for performance gains (faster serialization, less network traffic).