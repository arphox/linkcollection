## Tools
- [Gephi - The Open Graph Viz Platform](https://gephi.org/)
- [Attribute Changer](https://www.petges.lu/download/) - Change file and folder properties with ease
- [InstalledDriversList](http://www.nirsoft.net/utils/installed_drivers_list.html) - View the installed drivers list on Windows
- [nip.io](https://nip.io/) - wildcard DNS for any IP Address
- [x64dbg](https://x64dbg.com/) -  An open-source x64/x32 debugger for windows. 
- [glmcdona/Process-Dump](https://github.com/glmcdona/Process-Dump) - Windows reverse-engineering command-line tool to dump malware memory components back to disk for analysis
- [Luke Stackwalker](http://lukestackwalker.sourceforge.net/) - C/C++ code profiler using call stack sampling
- [Grabify IP Logger & URL Shortener](https://grabify.link/) -> ha valakinek meg akarnád szerezni az IP-jét így
- [Webhook.site - Test, process and transform emails and HTTP requests](https://webhook.site/)
- [Graphviz - Graph Visualization Software](http://www.graphviz.org/)
- [WebGraphviz - Graphviz in the Browser](http://www.webgraphviz.com/)
- [Gravizo - Your Graphviz, UMLGraph or PlantUML for your README](http://www.gravizo.com/)
- [Compiler Explorer](https://godbolt.org/) - bal oldalra lehet írni a C++ kódot, jobb oldalt mutatja az assembly megfelelőjét
- [CLOC -- Count Lines of Code](https://cloc.sourceforge.net/) - command line util megszámolni a kódsorokat pl. egy mappában.
- [Slidev – Presentation Slides for Developers](https://github.com/slidevjs/slidev) - markdown based presentation slides

## Measuring skill, competency, etc
- [Programmer Competency Matrix](https://sijinjoseph.com/programmer-competency-matrix/)
- [Engineering Ladders](http://www.engineeringladders.com/)

## Other
- [**Numbers Every Programmer Should Know By Year**](https://colin-scott.github.io/personal_website/research/interactive_latency.html)
- [opensource.guide](https://opensource.guide/)
- [StackOverflow Developer Survey](https://insights.stackoverflow.com/survey)
- [GitHub Language Stats - GitHut](https://madnight.github.io/githut) - ne csak a PULL REQUESTS-et nézd
- [Rico's cheatsheets](https://devhints.io/) - különböző nyelvekhez és cuccokhoz cheatsheet-ek
- [Krzysztof Kowalczyk's cheatsheets](https://referenceguide.dev/)
- [Visual Studio Dev Essentials (Microsoft)](https://my.visualstudio.com/Benefits)

## .NET / C#
- [SharpLab](https://sharplab.io/) - C#/VB/F# compiler playground

## Q&A sites
- [StackOverflow](https://stackoverflow.com/)
- [CoderWall](https://coderwall.com)

## Meetups
- [Meetup.com](https://www.meetup.com/)
