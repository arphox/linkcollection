# High Quality Software Engineering

I (Károly Ozsvárt) am proud to say that I am a quality-oriented software engineer.  
This means I prefer creating high quality solutions over lower quality ones.  
This is a resource collection on this topic.

## Articles
- [**Is High Quality Software Worth the Cost?**](https://martinfowler.com/articles/is-quality-worth-cost.html) - Martin Fowler, 2019

## Books
- **High-Quality Software Engineering** (Lessons from the Six-Nines World) - David Drysdale, 2007, free. [pdf](https://lurklurk.org/hqse.pdf), [webpage](https://lurklurk.org/hqse/hqse.html)