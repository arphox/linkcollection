1. [Teach Yourself Programming in Ten Years - Peter Norvig](http://www.norvig.com/21-days.html)
- [Developer Roadmaps](https://roadmap.sh/)

## How to become a better software developer
- [Become a Better Developer by Reading Source Code](https://www.stevejgordon.co.uk/become-a-better-developer-by-reading-source-code)
- [How To Rapidly Improve At Any Programming Language](https://www.cbui.dev/how-to-rapidly-improve-at-any-programming-language/)
- [google search](https://www.google.com/search?q=how+to+become+a+better+software+developer)

## Architect
- [Become a Better Software Architect](https://github.com/arphox/SoftwareArchitect)
