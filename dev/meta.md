- [The 10x Programmer Myth - Simple Thread](https://www.simplethread.com/the-10x-programmer-myth/)
- [Don't Distract New Programmers with OOP](https://prog21.dadgum.com/93.html)
- [20 controversial programming opinions « Software Engineering Stack Exchange Blog](https://programmers.blogoverflow.com/2012/08/20-controversial-programming-opinions/)
- [Learning to Ignore Superficially Ugly Code](https://prog21.dadgum.com/85.html) - I say it is a skill to be able to let go of ugly formatting and focus on the point **but** if given the opportunity, fix the formatting.
- [Write Code Like You Just Learned How to Program](https://prog21.dadgum.com/87.html)
- [Hopefully More Controversial Programming Opinions](https://prog21.dadgum.com/149.html)
- [Organizational Skills Beat Algorithmic Wizardry](https://prog21.dadgum.com/177.html)

## Time management
- [Declutter Your Work Day: 9 Tips to Manage your Tasks Without Stress | Michael's Coding Spot](https://michaelscodingspot.com/declutter/)
