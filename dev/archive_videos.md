# Watched videos
- [How and Why to Avoid Nil](https://www.destroyallsoftware.com/screencasts/catalog/how-and-why-to-avoid-nil)
- [Functional Core, Imperative Shell](https://www.destroyallsoftware.com/screencasts/catalog/functional-core-imperative-shell)
- [7 Secrets of Maintainable Codebases • Adam Tornhill • GOTO 2016](https://www.youtube.com/watch?v=0oDporwhToQ)
- [Scaling Yourself - Scott Hanselman | DevTernity 2019](https://www.youtube.com/watch?v=V4NJo2Mfvrc)
- [Vertical Slice Architecture - Jimmy Bogard | DevTernity 2019](https://www.youtube.com/watch?v=T6nglsEDaqA)
- [C# 7.2: Understanding Span](https://channel9.msdn.com/Events/Connect/2017/T125)
- [Eric Wastl — Advent of Code: Behind the Scenes | Øredev 2019](https://www.youtube.com/watch?v=bS9882S0ZHs)
- [Essential Truths Everyone Should Know about Performance in a Large Managed Codebase](https://channel9.msdn.com/Events/TechEd/Europe/2013/DEV-B333)

### NDC 2023
- [Writing Code with Code: Getting Started with the Roslyn APIs - Steve Gordon - NDC London 2023](https://www.youtube.com/watch?v=2AtNjxnwxZk) - mediocre
- [Is everything difficult, or is it just me? - Jo Franchetti - Copenhagen DevFest 2023](https://www.youtube.com/watch?v=n8YSjBEnf0Q)
- [Iron Man or Ultron: Is AI here to help us or hurt us? - Scott Hanselman - Copenhagen DevFest 2023](https://www.youtube.com/watch?v=RDVKl-27g9M)
- [Clean Code: Be The Hero - Ben Dechrai - NDC Oslo 2023](https://www.youtube.com/watch?v=Yu4kIgAH_NM)

### NDC 2022
- [ Down the Oregon Trail with Functional C# - Simon Painter - NDC Melbourne 2022](https://www.youtube.com/watch?v=STCaelGWEeA)

### GOTO 2021
- [Functional Programming Through the Lens of a Philosopher & Linguist • Anjana Vakil • GOTO 2021](https://www.youtube.com/watch?v=0kI-as3K4Zo)

### NDC 2021
- [Developer Fundamentals of Serverless NET Development with Azure Durable Functions - Jonah Andersson - YouTube](https://www.youtube.com/watch?v=C199S4R7cy8)

### NDC 2020
- [GraphQL, gRPC or REST? Resolving the API Developer's Dilemma - Rob Crowley](https://www.youtube.com/watch?v=l_P6m3JTyp0)
- [The Art of Code - Dylan Beattie - YouTube](https://www.youtube.com/watch?v=6avJHaC3C2U)
- [Change your habits: Modern techniques for modern C# - Bill Wagner](https://www.youtube.com/watch?v=aUbXGs7YTGo)

### GOTO 2019
- [Nullable Reference Types in C# 8 • Jon Skeet](https://www.youtube.com/watch?v=1tpyAQZFlZY)

### NDC 2019
- [Writing Allocation Free Code in C# - Matt Ellis](https://www.youtube.com/watch?v=nK54s84xRRs)
- [Clean Architecture with ASP.NET Core 3.0 - Jason Taylor - NDC Sydney 2019](https://www.youtube.com/watch?v=5OtUm1BLmG0)
- [Lowering in C#: What's really going on in your code? - David Wengier](https://www.youtube.com/watch?v=gc1AxbNybvw)

### NDC 2017
- [Avoiding Microservice Megadisasters - Jimmy Bogard - YouTube](https://www.youtube.com/watch?v=gfh-VCTwMw8)
- [Clean Coders Hate What Happens to Your Code When You Use These Enterprise Programming Tricks - YouTube](https://www.youtube.com/watch?v=FyCYva9DhsI) NDC 2017
