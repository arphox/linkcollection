## Quick tips
- [Don't ask to ask, just ask](https://dontasktoask.com/) - Don't _"Hi, can I ask a question?"_.
- [No Hello](http://nohello.com/) - Don't say just hello in chat
- [XY problem](https://xyproblem.info/) - Ask help with your _actual_ problem.

## Resources
- [How do I ask a good question? (StackOverflow)](https://stackoverflow.com/help/how-to-ask)

## Archive
- [How To Ask Questions The Smart Way](http://catb.org/~esr/faqs/smart-questions.html)
