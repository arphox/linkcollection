# IT
## Software engineering / development / programming
- [**Hacker News**](https://news.ycombinator.com/)
- [**freeCodeCamp** Programming Tutorials: Python, JavaScript, Git & More](https://www.freecodecamp.org/news/) - see also the main page
- [**.NET Blog** | Microsoft](https://devblogs.microsoft.com/dotnet/)
- [**Visual Studio Blog** | Microsoft](https://devblogs.microsoft.com/visualstudio/)
- [**Better Programming**](https://betterprogramming.pub/) - advice for programmers
- [**HackerNoon**](https://hackernoon.com/tagged/hackernoon-top-story) #hackernoon-top-story stories
- [**DZone** - Programming & DevOps news, tutorials & tools](https://dzone.com/)
- [**DEV.to**](https://dev.to/) - swarm of technology articles, quantity > quality
- [**Simple Programmer** - The Programming Blog to Help You Get a Job](https://simpleprogrammer.com/)
- [**fromdev** - A Technology Blog About Programming, Web Development, Books Recommendation, Tutorials and Tips for Developers](https://www.fromdev.com/)
- [**SubMain Blog** - Software Quality](https://blog.submain.com/) and other tech/dev articles
- [**.NET Core Tutorials**](https://dotnetcoretutorials.com/)
- [**Daily .NET Tips**](https://dailydotnettips.com/)
- [**DotNetCurry.com**: Learn C# .NET, ASP.NET Core MVC, Azure, DevOps, React, Vue, Angular, JavaScript | Tutorials for Beginners and Experienced Developers](https://www.dotnetcurry.com/)
- [**The Crazy Programmer** - Programming, Design and Development](https://www.thecrazyprogrammer.com/)
- [**Code as Craft** - Etsy's Engineering Blog](https://codeascraft.com/)
- [**Google Developers Blog**](https://developers.googleblog.com/)
- [**JetBrains Blog** | Developer Tools for Professionals and Teams](https://blog.jetbrains.com/category/news/)
- [**reddit.com/r/programming/**](https://www.reddit.com/r/programming/)
- [**reddit.com/r/coding/**](https://www.reddit.com/r/coding/)
- [**DotNetKicks**](https://dotnetkicks.com/)
- [**.NET Ketchup**](https://dotnetketchup.com/) - Catch up on the latest .NET news from around the world
- [**Medium/software-engineering**](https://medium.com/tag/software-engineering/top/week)
- [**Medium/programming**](https://medium.com/tag/programming/top/week)
- [**Medium/csharp**](https://medium.com/tag/csharp/latest)
- [**i-programmer.info**](https://www.i-programmer.info/) - Programming News and Views, book reviews, etc
- [**Discord Blog** / Engineering & Design](https://discord.com/category/engineering)
- [**Martin Fowler**](https://martinfowler.com/) - architecture, software craftmanship, techniques, patterns
- [**Robert C. Martin** - Clean Coder Blog](http://blog.cleancoder.com/) (see the main site too)
- [**Mark Seemann** - https://blog.ploeh.dk/](https://blog.ploeh.dk/) - programming, software development, and architecture. High quality.
- [**Itamar Turner-Trauring** - Code Without Rules ()](https://codewithoutrules.com) - many interesting articles in general
- [**Nick Craver** - Software Imagineering](https://nickcraver.com/blog/) - tech (mainly .NET) topics, mostly StackOverflow's great architecture
- [**Jiří Činčura** - Tabs over spaces](https://www.tabsoverspaces.com/) - C#, .NET and more
- [**Jeremy Likness** - Developer for Life](https://blog.jeremylikness.com/blog) - .NET, Azure, data
- [**Yegor Bugayenko** - Yegor's Blog About Computers](https://www.yegor256.com/) - generic tech/dev articles
- [**Jon Skeet** - Jon Skeet's coding blog](https://codeblog.jonskeet.uk/) - C#/.NET authority, Lord of StackOverflow
- [**Stephen Cleary**](https://blog.stephencleary.com/) - C# .NET MVP
- [**Andrew Lock**](https://andrewlock.net/) - C# .NET
- [**MAKOLYTE**](https://makolyte.com/category/csharp/) - C# .NET practical blog posts
- [**Steve Gordon** - Code with Steve - Adventures in .NET](https://www.stevejgordon.co.uk/) - C#, .NET and some interesting articles
- [**Vikram Chaudhary** - Dot Net For All - C#, .NET, Cloud, Programming and much more technical stuff](https://www.dotnetforall.com/)
- [**Gérald Barré** - Meziantou's blog](https://www.meziantou.net/) - blog about Microsoft technologies: .NET, WPF, UWP, TypeScript, etc.
- [**nietras** – Programming, mechanical sympathy, machine learning and .NET ❤.](https://nietras.com/)
- [**Jeff Atwood** - Coding Horror](https://blog.codinghorror.com/) - not just programming
- [**Vladimir Khorikov** - Enterprise Craftsmanship](https://enterprisecraftsmanship.com/) - unit testing, TDD, and more
- [**Eric St-Georges** - ESG](https://esg.dev/posts/) - C#/.NET and more
- [**Nikita Prokopov*](https://tonsky.me/) - programming and UX design
- [**Christian Nagel**](https://csharp.christiannagel.com/) - C#/.NET
- [**Michael Shpilt** - Michael's Coding Spot](https://michaelscodingspot.com/) - C#, debugging, performance
- [**Scott Hanselman**](https://www.hanselman.com/blog/) - .NET and more
- [**Eric Lippert** - Fabulous Adventures in Coding](https://ericlippert.com/) - C#, .NET and more
- [**Brian Chen** - betaveros](https://beta.vero.site/) and his [blog](https://blog.vero.site/) - competitive programmer, interesting links and reads
- [**Joel Spolsky** - Joel on Software](https://www.joelonsoftware.com/) - dev, meta and more
- [**Claire Novotny**](https://claires.site/) - Microsoft, agile, some tech
- [**Patrick McKenzie** - Kalzumeus Software](https://www.kalzumeus.com/) - general development topics
- [**Kamil Grzybek** - Programming and designing enterprise solutions with .NET](http://www.kamilgrzybek.com/) - Programming and designing enterprise solutions with .NET
- [**Dan Moore** - Letters To A New Developer – What I wish I had known when starting my development career](https://letterstoanewdeveloper.com/) - general advices for developers
- [**James Hague** - programming in the twenty-first century](https://prog21.dadgum.com/archives.html) - general programming topics, some computer science
- [**Matt Warren** - Performance is a Feature!](https://mattwarren.org) - C#/.NET performance
- [**Anders Abel** - Passion for Coding](https://coding.abel.nu/) - C#, .NET, EF, etc.
- [**Andras Nemes** - Exercises in .NET with Andras Nemes | Tips and tricks in C# .NET](http://dotnetcodr.com)
- [**Laszlo Deak**](https://blog.ladeak.net/) - personal experience on C# and .NET related topics
- [**Sean Sexton** - 2,000 Things You Should Know About C# | Everything a C# Developer Needs to Know, in Bite-Sized Chunks](https://csharp.2000things.com/) - last post - 2014
- [**Eran Sandler** - Advanced .NET Debugging](https://dotnetdebug.net/) - last post - 2011
- [**Vance Morrison**'s Weblog | Microsoft Docs](https://docs.microsoft.com/en-us/archive/blogs/vancem/) - .NET performance
- [**Simon schreibt**](https://simonschreibt.de/) - interesting articles about game development
- [**Amit Patel** - Red Blob Games](https://www.redblobgames.com/) - interactive visual explanations of math and algorithms, using motivating examples from computer games
- [**Arialdo Martini**](https://arialdomartini.github.io/) (arialdomartini)
- [**Urs Enzler** (Planet Geek)](https://www.planetgeek.ch/) - C# & F#
- [**Database Architects**](http://databasearchitects.blogspot.com/) - A blog by and for database architects.

### YouTube
- [**NDC Conferences** uploads - YouTube](https://www.youtube.com/playlist?list=UUTdw38Cw6jcm0atBPA39a0Q)
- [**DeepMind** - YouTube](https://www.youtube.com/c/DeepMind/videos) - Google Deepmind, AI, machine learning, etc
- [**Fireship** - YouTube](https://www.youtube.com/c/Fireship/videos) - short informational videos on technologies
- [**IAmTimCorey**](https://www.youtube.com/channel/UC-ptWR16ITQyYOglXyQmpzw) - C#/.NET videos
- [**Nick Chapsas**](https://www.youtube.com/c/Elfocrash) - C#/.NET videos
- [**SSW TV | Videos for developers, by developers**](https://www.youtube.com/c/SSWTV)


## Computer Science
- [**reddit.com/r/compsci/**](https://www.reddit.com/r/compsci/)
- [**GeeksforGeeks** | A computer science portal for geeks](https://www.geeksforgeeks.org/)



## Tech
- [**HWSW** Informatikai Hírmagazin](https://www.hwsw.hu/)
- [**PROHARDVER!** - Az Online PC Magazin](https://prohardver.hu)
- [**PC World**](https://pcworld.hu/) - informatika és tech



# Finance
- [**Kiszámoló** - egy blog a pénzügyekről](https://kiszamolo.hu/)
- [**Zsiday Viktor** befektetési blogja](http://www.zsiday.hu)
- [**KonyhaKontrolling** - YouTube](https://www.youtube.com/channel/UCF-graEyVYE6t1hSpudevPQ)



# Science
- [**Qubit**](https://qubit.hu/feed) - tudomány, tech, gazdaság



# General learning / interesting reads
- [**Farnam Street** - Learn faster, Think better, and make Smart Decisions](https://fs.blog/)
- [**Wait But Why**](https://waitbutwhy.com/archive) - interesting articles about everything
- [**Kevin Simler** - Melting Asphalt | Essays](https://meltingasphalt.com/archive/) - interesting essays on everything and a bit of computer science



# Email newsletters to subscribe
- [**C# Digest** - A weekly newsletter about C# and .NET](https://csharpdigest.net/)
- [**Awesome .NET Weekly** | LibHunt](https://dotnet.libhunt.com/newsletter)



# Podcasts
- [**.NET Rocks!**](https://www.dotnetrocks.com/)
- [**Hanselminutes Technology Podcast** - Fresh Air and Fresh Perspectives for Developers](https://www.hanselminutes.com/)
- [**The Stack Overflow Podcast**](https://stackoverflow.blog/podcast/)
- [**The Unhandled Exception Podcast**](https://unhandledexceptionpodcast.com/)
- [**Maintainable** podcast](https://www.maintainable.fm/) - "we speak with seasoned practitioners who have worked past the problems often associated with technical debt and legacy code"
- [**Coding Blocks**](https://www.codingblocks.net/) - "Podcast and Your Source to Learn How To Become a Better Programmer"

# Collections
- [ooh.directory](https://ooh.directory/) - Large collection of blogs about every topic
