## Tools
- [Excalidraw](https://excalidraw.com/) - intuitive online drawer
- [Noisli](https://www.noisli.com/)
  - also [Rainy Mood](https://www.rainymood.com/)
- [ZoomIt](https://docs.microsoft.com/en-us/sysinternals/downloads/zoomit) - a screen zoom and annotation tool for technical presentations
- [Epic Pen](https://epic-pen.com/) - draw on your screen, mroe heavy-weight than Zoomit
- [Ditto](https://ditto-cp.sourceforge.io/) - extension to the standard windows clipboard. If you want more than `Win+V` in Win10+.
- [Hemingway Editor](https://hemingwayapp.com/) - improve your writing with this in-browser app (or downloadable).
- [Prio](https://www.prnwatch.com/prio/) - Prio - Process Priority Saver for Windows

## Websites
- [12ft](https://12ft.io/): Remove popups, banners, and ads from any website. 