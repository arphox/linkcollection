# Walking
- [LPT: Don't under estimate walks](https://www.reddit.com/r/LifeProTips/comments/oxlhv7/lpt_dont_under_estimate_walks_walks_are_one_of/) ([mirror](https://web.archive.org/web/20210805133816/https://www.reddit.com/r/LifeProTips/comments/oxlhv7/lpt_dont_under_estimate_walks_walks_are_one_of/))
- [Even a little (10 min) exercise has benefits on the brain](https://www.inc.com/jessica-stillman/science-this-is-minimum-amount-of-exercise-to-boost-mental-performance.html)
