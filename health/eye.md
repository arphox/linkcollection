# Eye health

# 🇭🇺 Hungarian content

## Szemészeti leletek értelmezése
4 féle számot szoktak megadni mindkét szemre:
- **Visus**: látásélesség, ez kétféle lehet: korrigált és korrigálatlan. Korrigálatlan/nyers vízus esetén: amit a páciens szabad szemmel lát. Ez egy százalékos összefoglaló szám arról, hogy mennyire látsz jól. Az 1 (100%) a természetsen éles látás, és ahogy csökken úgy romlik, pl. 0.2 = 20%.
- **Sph** (vagy Dsph) azaz **szférikus dioptria**: a lencse nagyító erejét jelzi. Lehet + (közelre látsz rosszul) vagy - (távolra látsz rosszul) előjelű.
- **Cyl** (vagy Dcyl) azaz **cylinderes dioptria**: ez általában a szaruhártya formájából adódik. Felírható mínusz (-) és plusz (+) cylinderben is). Lehetséges, hogy valakinek csak cylinderes korrekcióra van szüksége.
- **Axis** (vagy Ax° vagy simán °): a **cylinder tengely**e, fokban jelölve. (0 és 180°közötti érték)

Az Sph, Cyl és Axis értékek összetartoznak, és más formában is felírhatóak.  
Pl. a következő két sor ugyanazt jelenti:
- SPH + 0,25 | CYL + 0,50 | AX 80
- SPH + 0,75 | CYL – 0,50 | AX 170

Ugyanis ha az SPH-hoz hozzáadod a CYL-t, a CYL előjelét negálod, és az AX-hez 90 fokkal eltolod (vagy +90 vagy -90, ahogy pozitív szám jön ki), akkor az ugyanaz. Bővebben itt: [A nagy cylinder-dilemma – Junior optika](https://www.junioroptika.com/a-nagy-cylinder-dilemma/) ([mirror](http://web.archive.org/web/20240908165456/https://www.junioroptika.com/a-nagy-cylinder-dilemma/)).