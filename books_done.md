# Elolvasott könyvek (Moly)

Az elolvasott könyveimet általánosságban a [Moly.hu profilomon](https://moly.hu/tagok/ozsvart_karoly/olvasmanylista) tartom számon, néhány kivétellel, amiket ebben a fájlban gyűjtök.

⚠️ Az újakat a lista elejére vedd fel.

# IT szakmai könyvek
- [Code That Fits in Your Head : Heuristics for Software Engineering - Mark Seemann](https://www.amazon.com/Code-That-Fits-Your-Head/dp/0137464401) ISBN-13: 978-0-13-746440-1
- [Refactoring - Improving the Design of Existing Code (Second Edition) - Martin Fowler](https://www.amazon.com/Refactoring-Improving-Existing-Addison-Wesley-Signature/dp/0134757599)  ISBN-13: 978-0134757599
- [High-Quality Software Engineering - David Drysdale](https://www.lurklurk.org/hqse/hqse.html)
- [CLR via C# (Fourth Edition) - Jeffrey Richter](https://www.amazon.com/CLR-via-4th-Developer-Reference/dp/0735667454) ISBN: 978-0-7356-6745-7
- [Dependency Injection in .NET - Mark Seemann](https://www.manning.com/books/dependency-injection-in-dot-net) ISBN: 9781935182504
- [Tiszta kód - Az agilis szoftverfejlesztés kézikönyve - Robert C. Martin](https://www.libri.hu/konyv/robert_c_martin.tiszta-kod.html) (eredeti cím: Clean Code: A Handbook of Agile Software Craftsmanship) ISBN (magyar): 9789639637696. ISBN (eredeti, angol könyvé): 9780132350884
- [Kódkönyv - A rejtjelezés és rejtjelfejtés története - Simon Singh](https://www.libri.hu/konyv/simon_singh.kodkonyv.html) ISBN: 9789635307982
- [C# programozás lépésről lépésre - Reiter István](https://reiteristvan.files.wordpress.com/2018/01/reiter-istvc3a1n-c-programozc3a1s-lc3a9pc3a9src591l-lc3a9pc3a9sre.pdf) ISBN: 9786155012174
- [C# jegyzet - Reiter István](https://reiteristvan.files.wordpress.com/2018/01/reiter-istvc3a1n-c-programozc3a1s-lc3a9pc3a9src591l-lc3a9pc3a9sre.pdf)
- [C# 2008 (TANTUSZ könyv)](https://www.libri.hu/konyv/stephen_randy_davis.c-2008-1.html) ISBN: 9789635455102
- [A C# programozási nyelv a felsőoktatásban (EKF-es)](http://csharptk.ektf.hu/download/csharp-ekf.pdf)
- [Programozás C# nyelven (2008-as változat) - Illés Zoltán](https://www.libri.hu/konyv/illes_zoltan.programozas-c-nyelven.html) ISBN: 9789638762948

# Egyéb könyvek
- 💵 [The Bogleheads' Guide to Investing](https://www.amazon.com/Bogleheads-Guide-Investing-Taylor-Larimore-dp-1118921283/dp/1118921283/) ISBN 978-1-118-92235-4 (ePDF)