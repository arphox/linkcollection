## Magyar
- [5 things I wish I'd known when learning piano [IMPORTANT] - YouTube](https://www.youtube.com/watch?v=jg_Nf_26R7c)
- [ZongoraÓra, III. rész, Játék két kézzel, dalok kísérettel - YouTube](https://www.youtube.com/watch?v=kh28h0HZ9XY)
- [ZongoraÓra, II. rész - Két kéz függetlenítése, skálagyakorlatok - YouTube](https://www.youtube.com/watch?v=k1-2AoDA8G4)
- [ZongoraÓra, I. rész - Alapok - YouTube](https://www.youtube.com/watch?v=0nJHeQIb-7U)

## YouTube csatornák
- [Become a Piano Superhuman](https://www.youtube.com/channel/UC1V2zI_VQ5et66WtKXhAXXA)
