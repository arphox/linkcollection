### Frédéric **Chopin**
- [List of compositions by Frédéric Chopin by opus number - Wikipedia](https://en.wikipedia.org/wiki/List_of_compositions_by_Fr%C3%A9d%C3%A9ric_Chopin_by_opus_number)

### Johann Sebastian **Bach**
- [List of compositions by Johann Sebastian Bach - Wikipedia](https://en.wikipedia.org/wiki/List_of_compositions_by_Johann_Sebastian_Bach)

### Carl **Czerny**
- [List of compositions by Carl Czerny - Wikipedia](https://en.wikipedia.org/wiki/List_of_compositions_by_Carl_Czerny)
