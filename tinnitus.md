# Informational videos
- [What’s that ringing in your ears? - Marc Fagelson](https://www.youtube.com/watch?v=TnsCsR2wDdk) - Ted-Ed, 2020
- [Tinnitus: Ringing in the Brain | Josef Rauschecker | TEDxCharlottesville](https://www.youtube.com/watch?v=XGq3MXQlRJs) - TEDx Talks, 2017

# History
Thinks I've read/watched.

- [A New CURE For Tinnitus? | OTO-313g](https://www.youtube.com/watch?v=QUEoi4RABsU) - Doctor Cliff, AuD, 2020
