## Kiszámoló által javasoltak:
Pénzügyi tanácsadáson (2023-03)
- ✅ [**The Bogleheads' Guide to Investing**](https://www.amazon.com/Bogleheads-Guide-Investing-Taylor-Larimore-dp-1118921283/dp/1118921283/)
- [**The Bogleheads' Guide to the Three-Fund Portfolio**: How a Simple Portfolio of Three Total Market Index Funds Outperforms Most Investors with Less Risk](https://www.amazon.com/Bogleheads-Guide-Three-Fund-Portfolio-Outperforms/dp/1119487331)
- [**The Ultimate Etf Guidebook**: A Comprehensive Guide to the World of Exchange-Traded Funds - Including the Latest Innovations and Ideas for ETF Portfolios](https://www.amazon.com/Ultimate-Etf-Guidebook-Comprehensive-Exchange-Traded/dp/0857197266)
- [**The MoneySense Guide to the Perfect Portfolio**](https://www.amazon.com/MoneySense-Guide-Perfect-Portfolio-2013-ebook/dp/B00G2FGMQ8/ref=cm_cr_arp_d_product_top?ie=UTF8)

Egyéb:
- [**The Millionaire Next Door**: The Surprising Secrets of America's Wealthy](https://www.amazon.com/Millionaire-Next-Door-Surprising-Americas/dp/1589795474) - Kiszámoló Akadémián javasolta

## Egyéb
- [**The Intelligent Investor** Rev Ed.: The Definitive Book on Value Investing](https://www.amazon.com/Intelligent-Investor-Definitive-Investing-Essentials/dp/0060555661)
- [**The Psychology of Money**: Timeless lessons on wealth, greed, and happiness](https://www.amazon.com/Psychology-Money-Timeless-lessons-happiness/dp/0857197681)
- [**Dollars and Sense**: How We Misthink Money and How to Spend Smarter](https://www.amazon.com/Dollars-Sense-Misthink-Money-Smarter/dp/006265120X)

## Magyar
- [**A lusta portfólió**](https://www.libri.hu/konyv/gyorgy_andras_142598.a-lusta-portfolio.html)
- [**Osztalékból ​szabadon**](https://moly.hu/konyvek/solyomi-david-osztalekbol-szabadon)
