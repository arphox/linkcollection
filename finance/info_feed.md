## Weboldalak
- [Kiszámoló](https://kiszamolo.hu/) - szerintem a legjobb magyar pénzügyi (és részben életviteli) blog.
- [DolgosPénz](https://dolgospenz.hu/) - általános pénzügyi cikkek, hasznos lehet, de csak kritikusan olvasd!
- [Zsiday Viktor befektetési blogja](http://www.zsiday.hu/blog). [RSS](http://zsiday.hu/rss).
- [KonyhaKontrolling](https://konyhakontrolling.hu/)
  - [YouTube](https://www.youtube.com/channel/UCF-graEyVYE6t1hSpudevPQ/videos) - egy alig ismert videós érdekes informatív témákkal
## YouTuberek
- [Bence Balázs](https://www.youtube.com/c/BenceBal%C3%A1zst%C5%91zsdebefektet%C3%A9smegtakar%C3%ADt%C3%A1s/videos) - tőzsde, befektetés, megtakarítás témában előadások érdekes (tenyérbemászó) stílusban.
