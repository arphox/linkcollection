- **George S. Clason -- Babilon leggazdagabb embere**: pénzügyi alapozó mesélősen, szórakoztatóan (e-bookban is van)
- [**Kiszámoló Akadémia**](https://www.youtube.com/playlist?list=PLuZt2T3kHxp15mbnjXNYCggArEY6FVnZn) - alapmű

### DolgosPénz
- [Itt kezdj – Indulj El Te Is A Pénzügyi Szabadság Felé!](https://dolgospenz.hu/itt-kezdj/)
- [Hogyan legyek gazdag, milliomos? 4 bevált út bemutatása (+9 bónusz tipp)](https://dolgospenz.hu/hogyan-legyek-gazdag/)
- [Pénzügyi szabadság: Hogyan érheted el 7 lépésben?](https://dolgospenz.hu/penzugyi-szabadsag/)
- [Top 35: A Leghatékonyabb Spórolási Tippek példákkal](https://dolgospenz.hu/sporolasi-tippek/)
- [A 25 legjobb passzív jövedelem forrás | DolgosPénz](https://dolgospenz.hu/passziv-jovedelem/)
- [Gyors pénzkeresés: Top 15 ötlet magyaroknak [példákkal]](https://dolgospenz.hu/gyors-penzkereses/)
- [Befektetés fogalma és alapjai kezdőknek: mi az és miért kell befektetni?](https://dolgospenz.hu/befektetes-kezdoknek/)
- [Mibe fektessem a pénzem? Legjobb 9 befektetési lehetőség](https://dolgospenz.hu/legjobb-befektetes/)
- [Részvény vásárlás online: útmutató lépésről lépésre](https://dolgospenz.hu/reszvenyvasarlas-online/)
- [ETF vásárlás menete lépésről lépésre](https://dolgospenz.hu/etf-vasarlas-menete/)

### ETF
- [ETF for beginners – Building assets with ETFs | justETF](https://www.justetf.com/en/academy/etf-for-beginners.html)

### Random Capital
- [Netboon](https://netboon.randomcapital.hu/)
- [**I**nstrumentum **K**ereső](https://randomcapital.hu/ik)
- [TBSZ nyitás](https://randomcapital.hu/termek/tbsz)
- [Első lépések](https://randomcapital.hu/elsolepesek)
  - [Netboon bemutató és kereskedési eszköztár - Webinárium](https://www.youtube.com/watch?v=4H1xWRZhJ3U&t=2907s)

### Hasznos
- [Personal Income Spending Flowchart - US](https://i.imgur.com/lSoUQr2.png)

### Kiszámoló
- [Megoldás, ha nem keresel eleget](https://kiszamolo.hu/megoldas-ha-nem-keresel-eleget/)
- [Az okos ember és a vihar](https://kiszamolo.hu/az-okos-ember-es-a-vihar/)
