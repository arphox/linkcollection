# Windows productivity

## Window handling
- `Win` + `Left` / `Right`: Snap the active window to the left or right side of the current monitor. You can press the keys again to move it between monitors, or snap it back to its original location.
- `Win` + `Up` / `Down`: Maximize or minimize the current window. If the window is currently snapped, this will also resize the window from its snapped position.

## Virtual desktops
- `Win` + `Tab`: Open Task view (virtual desktops)
- `Win` + `Ctrl` + `D`: Add new virtual desktop
- `Win` + `Ctrl` + `F4`: Close current virtual desktop
- `Win` + `Ctrl` + `Right arrow`: Move to the next virtual desktop (to the right)
- `Win` + `Ctrl` + `Left arrow`: Move to the next virtual desktop (to the left)

## Multi-monitor
- `Win` + `Shift` + `Left Arrow` / `Right Arrow`: Move selected window to the left/right monitor.