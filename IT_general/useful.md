## The big guys
- [**NirSoft utils**](https://www.nirsoft.net/utils/index.html) - many useful utilities
- [**Sysinternals**](https://learn.microsoft.com/en-us/sysinternals/) - Sysinternals utilities help you manage, troubleshoot and diagnose your Windows and Linux systems and applications.

## Websites
- [WeTransfer | Send Large Files Fast](https://wetransfer.com/)
- [Bear Blog](https://bearblog.dev/) - A privacy-first, no-nonsense, super-fast blogging platform
- [txti - Fast web pages for everybody](https://txti.es/)
- [How Well Can You Hear Audio Quality?](https://www.npr.org/sections/therecord/2015/06/02/411473508/how-well-can-you-hear-audio-quality)
- [ABX High Fidelity Test à la Tidal](http://abx.digitalfeed.net/)
- [High resolution music test website](http://www.2l.no/hires/)
- [Security Checklist](https://securitycheckli.st/)
- [StackEdit](https://stackedit.io/editor) - MarkDown editor
- [Gravizo](http://www.gravizo.com/) - Your Graphviz, UMLGraph or PlantUML for your README
- [LibreOps • radicalDNS](https://libreops.cc/radicaldns.html) - free, public DNS with ad and tracker filtering. Privacy friendly.
- https://12ft.io/ - Bypass any paywall, `https://12ft.io/<URL>`

## Applications
- [Big Stretch Reminder](https://monkeymatt.com/bigstretch/)
- [VeraCrypt](https://www.veracrypt.fr/en/Home.html) - Free Open source disk encryption with strong security for the Paranoid
- [LeoMoon SessionGuard](https://leomoon.com/downloads/desktop-apps/leomoon-sessionguard/) - stop your computer from sleeping or restarting without changing any settings on your computer
- [Microsoft Garage Mouse without Borders](https://www.microsoft.com/en-us/download/details.aspx?id=35460) - control multiple computer at once
- [D-LAN](https://www.d-lan.net/) - A free LAN file sharing software
- [Path Copy Copy](https://pathcopycopy.github.io/) - Copy file paths from Windows explorer's contextual menu
- [Meld](http://meldmerge.org/) - Visual diff and merge tool
- [ClipX](http://bluemars.org/clipx/) - free, tiny clipboard history manager
- [SyncThing](https://syncthing.net/) - file synchronization between two or more computers in real time, without dedicated servers (peer to peer).
- Bootable USB drive makers
  - [**Ventoy**](https://www.ventoy.net/) -  A new bootable USB solution ([github](https://github.com/ventoy/Ventoy))
  - [Rufus](https://rufus.ie/en/) - Create bootable USB drives the easy way
  - [Etcher](https://etcher.balena.io/) - Flash OS images to SD cards & USB drives

## List of "paste" sites
- https://topaz.github.io/paste/ - A no-datastore, **client-side** paste service.

## File upload sites
- [Catbox](https://catbox.moe/) - max 200 MB, [compatible with ShareX](https://catbox.moe/sharexcode.txt)
