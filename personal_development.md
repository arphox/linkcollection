- [15 Life Changing Ideas (2018)](https://medium.com/@dariusforoux/15-life-changing-ideas-ceeece7f4ca0)
- [The Productivity Paradox: How Working Less Will Make You More Productive (2018)](https://hackernoon.com/the-productivity-paradox-how-working-less-will-make-you-more-productive-b7c6d3df442c)
- [15 Brain Exercises to Keep Your Mind Sharp](https://bebrainfit.com/brain-exercises/)
- [Here’s the Simplest Trick Ever to Become More Productive](https://www.njlifehacks.com/measure-to-improve-productivity/) - _"What gets measured gets improved."_
- [14 Ways to Boost Your Productivity](https://www.njlifehacks.com/productivity-tips-backed-by-science/)

---

- [The test that reveals your hidden strengths | Laurie Santos - YouTube](https://www.youtube.com/watch?v=MeHsqm4F-W4)
