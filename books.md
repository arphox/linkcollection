## egyéb
- ezt nagyon kéne: [Engineering Fundamentals Playbook](https://microsoft.github.io/code-with-engineering-playbook/)
- +[Algorithms to Live By: The Computer Science of Human Decisions](https://www.goodreads.com/book/show/25666050-algorithms-to-live-by)
- Getting Things Done
- Nuclear War Survival Skills
- [Az orchideák bolygója](https://moly.hu/konyvek/herbert-w-franke-az-orchideak-bolygoja)
- [The Mind Illuminated: A Complete Meditation Guide Integrating Buddhist Wisdom and Brain Science for Greater Mindfulness Paperback – January 3, 2017](https://www.amazon.com/Mind-Illuminated-Meditation-Integrating-Mindfulness/dp/1501156985) - Deák Gyuri ajánlotta, google drive-ban
- [The Formula: The Universal Laws of Success (Albert-László Barabási)](https://www.amazon.com/Formula-Universal-Laws-Success/dp/0316505498) - sokak által nagyon dícsért könyv
- [Thinking, Fast and Slow (Daniel Kahneman)](https://www.amazon.com/Thinking-Fast-Slow-Daniel-Kahneman/dp/0374533555) - leszedve
- [The Pleasure of Finding Things Out: The Best Short Works of Richard P. Feynman](https://www.amazon.co.uk/dp/0465023959)
- ["What Do You Care What Other People Think?": Further Adventures of a Curious Character](https://www.amazon.co.uk/dp/0393355640)
- [Surely You're Joking Mr Feynman: Adventures of a Curious Character as Told to Ralph Leighton](https://www.amazon.co.uk/Surely-Youre-Joking-Feynman-Adventures/dp/009917331X/)
- [The Meaning of It All: Thoughts of a Citizen-Scientist](https://www.amazon.co.uk/dp/0465023940)
- [Deep Work: Rules for Focused Success in a Distracted World](https://www.amazon.com/Deep-Work-Focused-Success-Distracted/dp/1455586692) - Dan Moore - Letters to a New Developer könyvből
- [The Psychology of Money: Timeless lessons on wealth, greed, and happiness](https://www.amazon.com/Psychology-Money-Timeless-lessons-happiness/dp/0857197681) - van magyarul is, pénzügyi alap
- Hachette India The Design Of Everyday Things: Revised And Expanded Edition
- Az ideális csapatjátékos
- [Dr. Bo's Critical Thinking Series](https://www.amazon.com/Dr-Bos-Critical-Thinking-Series/dp/B09N2QQJCF)
- Eric Berne könyvei: Emberi játszmák, Sorskönyv
- Lock Gareth: Under Pressure: Diving Deeper with Human Factors – [amazon](https://www.amazon.com/Under-Pressure-Diving-Deeper-Factors/dp/199958497X), [weboldal](https://www.thehumandiver.com/underpressure)
- Varga Tamás (Betsson GI dev) ajánlotta: Open Awareness Open Mind: Finding lasting peace with the practice of meditation
- Earl Swift - Big roads könyv
- [M. Scott Peck - A ​járatlan út](https://moly.hu/konyvek/m-scott-peck-a-jaratlan-ut)

Kívánságlista:
- [59 Seconds: Change Your Life in Under a Minute](https://www.amazon.com/59-Seconds-Change-Under-Minute/dp/0307474860)
