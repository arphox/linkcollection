## 1. titok - Légy hű önmagadhoz!
**Állítások**
- Válaszd az éberséget!
- Találj rá a sorsodra!
- Gyakran reflektálj az életedre!
- A szívünk követése bátorságot igényel, lehetnek nagyon nehezek is a döntések!

**Kérdések**
- Az elmúlt hét olyan volt, amilyennek szerettem volna?
  - Mitől érezném a következő hetet igazabbnak?
- Azokra a dolgokra összpontosítok, amik valóban fontosak számomra?
- Olyan ember voltam ezen a héten, amilyen lenni szeretnék?
  - Mit tehetnék, hogy a következő héten közelebb kerüljek ahhoz, aki lenni szeretnék?
- A szívemet követve élek és hű vagyok önmagamhoz?
  - Jelenlegi élethelyzetemben mi jelentené azt, hogy valóban a szívemre hallgatok?
    
*"Az élet legnagyobb tragédiája, ha egész életedben halászol, és csak a végén jössz rá, hogy igazából nem is halat akartál."*


## 2. titok - Élj úgy, hogy ne kelljen megbánnod!
**Állítások**
- Kockáztass többet, hogy végül ne bánj semmit!
- A legjobb forgatókönyvhöz vezető utat válaszd!
- Engedd el! A hibákat, a rossz emlékeket, az aggodalmat, stb...

**Kérdések**
- Ezen a héten félelemből cselekedtem? Hogyan lehetnék bátrabb a következő héten?
- A hitem, meggyőződésem szerint cselekedtem ezen a héten?
  - Hogyan valósíthatnám meg még inkább mindazt, amiben hiszek?
- Mi az, amit most rögtön megtennék, ha a félelem helyett a bátorság irányítaná a cselekedeteimet?
  - Mit tennék máshogy, ha a verandán üldögélő idős önmagam szemével tekintenék az életemre?
- Hogyan reagálok a kudarcokra? Továbblépek vagy visszavonulok?

*"A sír fölött állva a ki nem mondott szavak és az elmaradt tettek miatt ejtjük a legkeservesebb könnyeket."*


## 3. titok - Válaszd a szeretetet!
**Állítások**
- A szeretet döntés!
- Szeresd önmagad!
- Dönts úgy, hogy jóindulattal tekintesz másokra!
- Ha teheted, tégy jót és sose bánts másokat!

**Kérdések**
- Időt szakítottam ezen a héten a barátokra, a családra és kapcsolataim ápolására?
  - Az emberek fontosabbak voltak, mint a tárgyak?
- Szeretettel és kedvességgel fordultam a hozzám közel állókhoz?
  - Mit tehetnék a jövő héten, hogy még több szeretetet adjak nekik?
- Szeretetet és jóindulatot sugároztam a világba ezen a héten?
  - Sikerült úgy élnem, hogy minden ismeretlenben meglássam azt, akinek az életébe talán változást hozhatok?
- Melyik farkast etettem ezen a héten?
  - Olyan emberekkel töltöttem az időmet, akik felemelő hatással vannak rám?
  - Szeretettel közeledtem magam felé?
  - Mennyire merültem bele a negatív belső monológba, és önhipnózisba?
  - Virágok vagy gaz magjait vetettem el a tudatalattimban?
    

## 4. titok - Élj a jelenben!
**Állítások**
- Minden új nap ajándék, tekinthetünk rá teljes különálló életként!
- Élj úgy, mintha ma látnád utoljára a naplementét!
- Válaszd a hálát!
- Élj meg minden pillanatot, légy ott az "első sorban", add át magad neki teljesen!

**Kérdések**
- Sikerült ezen a héten teljesen kiélveznem, bármit is csináltam?
  - Valóban "jelen" voltam, vagy csak a testem volt itt?
- Megragadtam minden örömet, ami megadatott ezen a héten, "beleszagoltam a virágokba"?
  - Tudatosan jártam az életem útját, vagy csak átszaladtam rajta?
- Mi az, amiért ezen a héten hálás lehetek?
  - Belecsúsztam abba a gondolkodásba, hogy "boldog leszek, ha..."?
  - A megelégedettséget és a boldogságot választottam?
- A jelenben éltem a héten, vagy hagytam hogy a holnap és a tegnap ellopja a ma boldogságát?


## 5. titok - Többet adj, mint amennyit elveszel!
**Állítások**
- A lehető legnagyobb boldogság mindig abból származik, amit adsz, és nem abból amit kapsz.
- Kérdezd meg, mit vár tőled az élet!
- Nem tudhatod, hogy egy szavad, tetted mekkora nagy hatással van a másikra. Lehet, hogy elenyésző; de lehet hogy az életét változtatod meg!
- A világ miatt bánkódj, ne magad miatt! Szolgáld a világot!

**Kérdések**
- Hozzájárultam valamivel ezen a héten ahhoz, hogy jobb világban éljünk?
- Tudatosítottam, hogy az én ittlétem is számít, még ha ezt nem is veszem észre?
- Kedves, nagylelkű, adakozó voltam ezen a héten? Mit tehetnék, hogy a jövő héten még inkább ilyen legyek?
- Mire összpontosítottam ezen a héten:
  - a "kis én" szükségleteire: tárgyak, státusz, hatalom **vagy**
  - a "magasabb én" vágyára: hozzájárulni ahhoz, hogy jobb világban éljünk?

*"Akkor kezd el élni az ember, amikor képes felülemelkedni saját nehézségeinek beszűkült valóságán, és az egész emberiségre ható gondokkal foglalkozik"*

**Mit tehetnék, hogy a következő héten mélyebben megéljem ezeket a titkokat?**