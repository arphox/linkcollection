### Dijkstra
- [Dijkstra's Algorithm - Computerphile - YouTube](https://www.youtube.com/watch?v=GazC3A4OQTE)
- [Graph Data Structure 4. Dijkstra’s Shortest Path Algorithm - YouTube](https://www.youtube.com/watch?v=pVfj6mxhdMw)

## Archive
- [The Most Basic Pathfinding Algorithm, Explained - YouTube](https://www.youtube.com/watch?v=rbYxbIMOZkE)
