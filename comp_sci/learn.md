## General
- [Teach Yourself Computer Science](https://teachyourselfcs.com/)
- [Data Structures and Algorithms Problems – Techie Delight](https://www.techiedelight.com/data-structures-and-algorithms-problems/)

## Algorithms
- [Introduction to Algorithms (MIT OpenCourseWare)](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/index.htm)
- [Algorithms, 4th Edition by Robert Sedgewick and Kevin Wayne](https://algs4.cs.princeton.edu/home/) - online, free

## Courses
- [Algorithms, Part I | Coursera](https://www.coursera.org/learn/algorithms-part1)
- [Algorithms, Part II | Coursera](https://www.coursera.org/learn/algorithms-part2)
